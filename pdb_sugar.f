       module my_subs

       implicit none

       contains

       FUNCTION cross(a, b)
          DOUBLE PRECISION, DIMENSION(3) :: cross
          DOUBLE PRECISION, DIMENSION(3), INTENT(IN) :: a, b

          cross(1) = a(2) * b(3) - a(3) * b(2)
          cross(2) = a(3) * b(1) - a(1) * b(3)
          cross(3) = a(1) * b(2) - a(2) * b(1)
       END FUNCTION cross

        end module my_subs

      program pdb_kplot
c     This program takes a pdb file and converts to kplot
c     Modified to read only backbone for comparison. 
c     N. Sridhar -----------Nov, 2013
c     requirements: coords.pdb
c     usage: pdb_kplot
c     output: coords.kpl
      use my_subs
      implicit none
      
c     PDB file structure is constant for all pdb files!
c     COLUMNS  DATA TYPE     FIELD       DEFINITION
c     --------------------------------------------------------------------------
c     1 -  6  Record name   "ATOM  "
c     7 - 11  Integer       serial      Atom serial number.
c     13 - 16  Atom          name        Atom name.
c     17       Character     altLoc      Alternate location indicator.
c     18 - 20  Residue name  resName     Residue name.
c     22       Character     chainID     Chain identifier.
c     23 - 26  Integer       resSeq      Residue sequence number.
c     27       AChar         iCode       Code for insertion of
c     residues.
c     31 - 38  Real(8.3)     x           Orthogonal coordinates for X,
c     Angstroms.
c     39 - 46  Real(8.3)     y           Orthogonal coordinates for Y,
c     Angstroms.
c     47 - 54  Real(8.3)     z           Orthogonal coordinates for Z,
c     Angstroms.
c     55 - 60  Real(6.2)     occupancy   Occupancy.
c     61 - 66  Real(6.2)     tempFactor  Temperature factor.
c     73 - 76  LString(4)    segID       Segment identifier,
c     left-justified.
c     77 - 78  LString(2)    element     Element symbol,
c     right-justified.
c     79 - 80  LString(2)    charge      Charge on the atom.
c     
      
c     include 'varcom.inc'
      integer idmy1, idmy2, i1, numatoms, ios, num_ca, num_h
      integer at1,at2,at3,at4,at5,at6
      integer at1_t1,at2_t1,at3_t1,at4_t1
      integer at1_t2,at2_t2,at3_t2,at4_t2              
      integer atom_num (1000), natoms_res
      integer i,count,j
      character (4) atom_name(1000)
      character (4) res_name(1000)
      integer (4) res_seq(1000), maxatoms
c     integer res_seq
      character (10) cdmy1, cdmy2, cdmy3,test
      character (10) cdmy4, cdmy5, cdmy6, cdmy20
      character (10) cdmy7, cdmy8, cdmy9, cdmy10
      character (4) atomname
c     parameter  numatoms = 23
c     real coordinates
      double precision X(1:1000,1:3)
      double precision L,M
c     com coordinates
      double precision X_res(1:1000,1:3), X_bc(1:1000,1:3)
      double precision X_phi(1:100,1:3)
      double precision X_psi(1:100,1:3)
      double precision AB(3), AC(3)
      double precision pl_a,pl_b,pl_c,pl_d
      double precision cross1(3),dot1,norm(3),pt1(3)
      double precision B1(3),B11
      double precision B2(3),B22
      double precision B3(3),B33
      double precision B4(3)
      double precision N1(3),N2(3),phi,psi,M1(3),B2n(3),B3n(3)
      double precision B1n(3),B4n(3)
c     alpha carbon coordinates
      double precision magN1,magN2,magB2,magB3
      double precision magB1,magB4
      double precision dist(1000),dist1
      double precision crossN1(3),crossN2(3)
      character (len = 100) arg
      character ( len = 200 ) string, line
      character ( len = 200 ) cmd
      
c     Define hbond length as 2.6 Angstroms
c     hbond_length = 2.6        
      
c     convert .inpcrd file to .pdb
c     cmd = 'ambpdb -p coords.prmtop<coords.inpcrd > coords.pdb'
c     call system (cmd)
c     write (*,*)'**************************************************'
c     write (*,*)'NS, Jan, 2014. pdb_kplot converter'
c     write (*,*)'**************************************************'
      CALL getarg(1, arg)
      open (unit = 7, file = arg, status = 'old')
      open (unit = 8,file ='torsions.dat',access='append',
     &status = 'unknown')
      write (*,*) "Enter total number of atoms" 
      read (*,*) numatoms
      write (*,*) "Enter 6 atom numbers that belong to ring(first 4 in
     &      plane)"
      at1 = 0
      read (*,*) at1,at2,at3,at4,at5,at6
      write (*,*) "Enter 4 atoms for first torsion"
      read(*,*) at1_t1,at2_t1,at3_t1,at4_t1
      write (*,*) "Enter 4 atoms for second torsion"
      read(*,*) at1_t2,at2_t2,at3_t2,at4_t2              
      maxatoms = 1000
c     count number of atoms
      count = 0
      i = 1   
      do idmy1 = 1,numatoms
c     read all lines in pdb into a string
         read ( 7, '(a)', iostat = ios ) string
c     write (*,*) trim(string)
         
c     sort all ATOMS out
         if ( string(1:4) .eq. 'ATOM' ) then
            
            i = i+1  
c     write (*,*) trim(string)
c     assign values to x,y and z coordinates here
c     read is a trick to convert string to double or other formats
            cdmy1 = (trim(string(31:38)))
            read (cdmy1, *) X(i,1)
c     write (*,*) X(i,1)
            cdmy2 = (trim(string(39:46)))
            read (cdmy2, *) X(i,2)
            cdmy3 = (trim(string(47:54)))
            read (cdmy3, *) X(i,3)
            cdmy4 = (trim(string(7:11))) 
            read (cdmy4, *) atom_num(i)
            cdmy5 = (trim(string(13:16)))
            read (cdmy5, *) atom_name(i)
c           write(*,*) atom_name(i)
            
            cdmy6 = (trim(string(23:26)))
            read (cdmy6, *) res_seq(i)
            cdmy7 = (trim(string(18:20)))
            read (cdmy7, *) res_name(i)
            
c     print only backbone for comparison with other structures.
c     turn off if statement by commenting out the if section.

             if (( atom_num(i) .eq. at1).or.
     &          (atom_num(i) .eq. at2).or.
     &          (atom_num(i) .eq. at3).or.
     &          (atom_num(i) .eq. at4).or.
     &          (atom_num(i) .eq. at5).or.
     &          (atom_num(i) .eq. at6))then
c          write(*,*) "dist",  atom_num(i),i,atom_name(i)
              X_res(i,1) = X(i,1)
              X_res(i,2) = X(i,2)
              X_res(i,3) = X(i,3) 
             endif
c     phi read from input
             if (( atom_num(i) .eq. at1_t1).or.
     &          (atom_num(i) .eq. at2_t1).or.
     &          (atom_num(i) .eq. at3_t1).or.
     &          (atom_num(i) .eq. at4_t1))then

              write(*,*) 'Reading phi for atoms'
                
                      do j = 1,3
               X_phi(i,j)= X(i,j)
c              write (*,*)"tor", X_phi(i,j), atom_num(i),i,atom_name(i),
c    &         res_name(i)
                      enddo

            endif


             if (( atom_num(i) .eq. at1_t2).or.
     &          (atom_num(i) .eq. at2_t2).or.
     &          (atom_num(i) .eq. at3_t2).or.
     &          (atom_num(i) .eq. at4_t2))then

              write(*,*) 'Reading phi for atoms'
                
                      do j = 1,3
               X_psi(i,j)= X(i,j)
c              write (*,*)"tor", X_psi(i,j), atom_num(i),i,atom_name(i),
c    &         res_name(i)
                      enddo

            endif



      endif
      enddo

c    Determine ring atoms :
     

      write(*,*) "RING1 - ","|",atom_name(at1+1),atom_name(at2+1),
     & atom_name(at3+1),"|",atom_name(at4+1),"|",atom_name(at5+1),
     & atom_name(at6+1)
 

c   Equation of plane: AB, AC, (ABcrossAC).

c           write(*,*) X_res(at6+1,1), at6,atom_name(at6+1)
c    Vector1: AB 
      AB(1) = X_res(at1+1,1) - X_res(at2+1,1)
      AB(2) = X_res(at1+1,2) - X_res(at2+1,2)
      AB(3) = X_res(at1+1,3) - X_res(at2+1,3)     
c    AC
      AC(1) = X_res(at3+1,1) - X_res(at2+1,1)
      AC(2) = X_res(at3+1,2) - X_res(at2+1,2)
      AC(3) = X_res(at3+1,3) - X_res(at2+1,3)

c   AB X AC. Coefficients of plane equation ax+by+cz+d = 0
      cross1 = cross(AB,AC)
      pl_a = cross1(1)
      pl_b = cross1(2)
      pl_c = cross1(3)

      pl_d = -(pl_a*X_res(at4+1,1) + pl_b*X_res(at4+1,2) +
     &  pl_c*X_res(at4+1,3))

      write (*,*) "Plane coefficients =",pl_a,pl_b,pl_c,pl_d

c  5th and 6th atom should be outside plane. Take dot product of (a,b,c)
c  and point.
c  both same sign => boat. opp sign => chair 
      norm = (/pl_a,pl_b,pl_c/)
      pt1 = (/X_res(at5+1,1),X_res(at5+1,2),X_res(at5+1,3)/)
      dot1 = dot_product(norm,pt1)
      write(*,*) "dot1 = ", dot1, atom_name(at5+1), "plane" 
      pt1 = (/X_res(at6+1,1),X_res(at6+1,2),X_res(at6+1,3)/)
      dot1 = dot_product(norm,pt1)
      write(*,*) "dot2 = ", dot1, atom_name(at6+1), "plane" 


c  Torsions:
c     calculate vectors b1,b2,b3 for torsions from point identified
cbove. 

           write (*,*) "----------------TORSIONS--------------------"
           write (*,*) "PHI:First set of atoms read from
     &  input in this order:"
	   write (*,*) atom_name(at1_t1+1), res_name(at1_t1+1)
           write (*,*) atom_name(at2_t1+1), res_name(at2_t1+1)
           write (*,*) atom_name(at3_t1+1), res_name(at3_t1+1)
           write (*,*) atom_name(at4_t1+1), res_name(at4_t1+1)

           B1(1) = X_phi(at2_t1+1,1) - X_phi(at1_t1+1,1) 
           B1(2) = X_phi(at2_t1+1,2) - X_phi(at1_t1+1,2) 
           B1(3) = X_phi(at2_t1+1,3) - X_phi(at1_t1+1,3) 

c          02(glycosidic)-C1G

           B2(1) = X_phi(at3_t1+1,1) - X_phi(at2_t1+1,1) 
           B2(2) = X_phi(at3_t1+1,2) - X_phi(at2_t1+1,2) 
           B2(3) = X_phi(at3_t1+1,3) - X_phi(at2_t1+1,3) 

c          C2f-O2g(glycosidic)
           B3(1) = X_phi(at4_t1+1,1) - X_phi(at3_t1+1,1) 
           B3(2) = X_phi(at4_t1+1,2) - X_phi(at3_t1+1,2) 
           B3(3) = X_phi(at4_t1+1,3) - X_phi(at3_t1+1,3) 

c        normalise all

           magB1 = sqrt(B1(1)**2 + B1(2)**2 + B1(3)**2)
           B1n(1) = B1(1)/magB1
           B1n(2) = B1(2)/magB1
           B1n(3) = B1(3)/magB1

           magB2 = sqrt(B2(1)**2 + B2(2)**2 + B2(3)**2)
           B2n(1) = B2(1)/magB2
           B2n(2) = B2(2)/magB2
           B2n(3) = B2(3)/magB2                                 

           magB3 = sqrt(B3(1)**2 + B3(2)**2 + B3(3)**2)
           B3n(1) = B3(1)/magB3
           B3n(2) = B3(2)/magB3
           B3n(3) = B3(3)/magB3 

c          magB4 = sqrt(B4(1)**2 + B4(2)**2 + B4(3)**2)
c          B4n(1) = B4(1)/magB4
c          B4n(2) = B4(2)/magB4
c          B4n(3) = B4(3)/magB4 


c      normalized cross products N1,N2 N1 = <B1 x B2> N2 = <B2 x B3>
           
          crossN1 = cross(B1n,B2n)
          crossN2 = cross(B2n,B3n)

          magN1 = sqrt(crossN1(1)**2 + crossN1(2)**2 + crossN1(3)**2)
          magN2 = sqrt(crossN2(1)**2 + crossN2(2)**2 + crossN2(3)**2)

          N1(1) = crossN1(1)
          N1(2) = crossN1(2)
          N1(3) = crossN1(3)
          N2(1) = crossN2(1)
          N2(2) = crossN2(2)
          N2(3) = crossN2(3)

c      m1 = N1 x <b2>
          M1 = cross(N1,B2n)
c      Finally L = N1.N2,  M = M1.N2 
         L = dot_product(N1,N2)
         M = dot_product(M1,N2)
c         write(*,*) "test",test
         phi = atan2(M,L)
         write (*,*) "Phi = ", phi*57.2957795

c        PSI:
c        reset vector values to new atoms
         do i = 1,3
          N1(i) = 0
          N2(i) = 0
          B1(i) = 0
          B2(i) = 0
          B3(i) = 0
          B1n(i) = 0
          B2n(i) = 0
          B3n(i) = 0
         enddo 
          
       write (*,*) "PSI:Set of atoms read from input in this order:"
       write (*,*) atom_name(at1_t2+1), res_name(at1_t2+1)
       write (*,*) atom_name(at2_t2+1), res_name(at2_t2+1)
       write (*,*) atom_name(at3_t2+1), res_name(at3_t2+1)
       write (*,*) atom_name(at4_t2+1), res_name(at4_t2+1)

           B1(1) = X_psi(at2_t2+1,1) - X_psi(at1_t2+1,1)
           B1(2) = X_psi(at2_t2+1,2) - X_psi(at1_t2+1,2)
           B1(3) = X_psi(at2_t2+1,3) - X_psi(at1_t2+1,3)

c          02(glycosidic)-C1G

           B2(1) = X_psi(at3_t2+1,1) - X_psi(at2_t2+1,1)
           B2(2) = X_psi(at3_t2+1,2) - X_psi(at2_t2+1,2)
           B2(3) = X_psi(at3_t2+1,3) - X_psi(at2_t2+1,3)

c          C2f-O2g(glycosidic)
           B3(1) = X_psi(at4_t2+1,1) - X_psi(at3_t2+1,1)
           B3(2) = X_psi(at4_t2+1,2) - X_psi(at3_t2+1,2)
           B3(3) = X_psi(at4_t2+1,3) - X_psi(at3_t2+1,3)

c        normalise all

           magB1 = sqrt(B1(1)**2 + B1(2)**2 + B1(3)**2)
           B1n(1) = B1(1)/magB1
           B1n(2) = B1(2)/magB1
           B1n(3) = B1(3)/magB1

           magB2 = sqrt(B2(1)**2 + B2(2)**2 + B2(3)**2)
           B2n(1) = B2(1)/magB2
           B2n(2) = B2(2)/magB2
           B2n(3) = B2(3)/magB2

           magB3 = sqrt(B3(1)**2 + B3(2)**2 + B3(3)**2)
           B3n(1) = B3(1)/magB3
           B3n(2) = B3(2)/magB3
           B3n(3) = B3(3)/magB3


c      normalized cross products N1,N2 N1 = <B1 x B2> N2 = <B2 x B3>

          crossN1 = cross(B1n,B2n)
          crossN2 = cross(B2n,B3n)

          magN1 = sqrt(crossN1(1)**2 + crossN1(2)**2 + crossN1(3)**2)
          magN2 = sqrt(crossN2(1)**2 + crossN2(2)**2 + crossN2(3)**2)

          N1(1) = crossN1(1)
          N1(2) = crossN1(2)
          N1(3) = crossN1(3)
          N2(1) = crossN2(1)
          N2(2) = crossN2(2)
          N2(3) = crossN2(3)

c      m1 = N1 x <b2>
          M1 = cross(N1,B2n)
c      Finally L = N1.N2,  M = M1.N2 
         L = dot_product(N1,N2)
         M = dot_product(M1,N2)
c         write(*,*) "test",test
         psi = atan2(M,L)
         write (8,*) phi*57.2957795,psi*57.2957795, arg
         write (*,*) "PSI",psi*57.2957795, arg



c  
      do i=1,numatoms   
        do j = i+1,numatoms
          if ((X_res(i,1) .ne. 0).and.(X_res(j,1).ne.0)) then
          dist1 = (X(i,1)-X(j,1))**2+(X(i,2)-X(j,2))**2+
     &           (X(i,3) - X(j,3))**2
          dist(i) = sqrt(dist1)
c         write(*,*),atom_name(i),atom_name(j),dist(i),res_name(i),i
          endif
        enddo
      enddo

         
         write(*,*) "Max dist.", maxval(dist),trim(arg)

      
      end
