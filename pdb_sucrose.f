       module my_subs

        implicit none

       contains

       FUNCTION cross(a, b)
          DOUBLE PRECISION, DIMENSION(3) :: cross
          DOUBLE PRECISION, DIMENSION(3), INTENT(IN) :: a, b

          cross(1) = a(2) * b(3) - a(3) * b(2)
          cross(2) = a(3) * b(1) - a(1) * b(3)
          cross(3) = a(1) * b(2) - a(2) * b(1)
       END FUNCTION cross

        end module my_subs

      program pdb_kplot
c     This program takes a pdb file and converts to kplot
c     Modified to read only backbone for comparison. 
c     N. Sridhar -----------Nov, 2013
c     requirements: coords.pdb
c     usage: pdb_kplot
c     output: coords.kpl
      use my_subs
      implicit none
      
c     PDB file structure is constant for all pdb files!
c     COLUMNS  DATA TYPE     FIELD       DEFINITION
c     --------------------------------------------------------------------------
c     1 -  6  Record name   "ATOM  "
c     7 - 11  Integer       serial      Atom serial number.
c     13 - 16  Atom          name        Atom name.
c     17       Character     altLoc      Alternate location indicator.
c     18 - 20  Residue name  resName     Residue name.
c     22       Character     chainID     Chain identifier.
c     23 - 26  Integer       resSeq      Residue sequence number.
c     27       AChar         iCode       Code for insertion of
c     residues.
c     31 - 38  Real(8.3)     x           Orthogonal coordinates for X,
c     Angstroms.
c     39 - 46  Real(8.3)     y           Orthogonal coordinates for Y,
c     Angstroms.
c     47 - 54  Real(8.3)     z           Orthogonal coordinates for Z,
c     Angstroms.
c     55 - 60  Real(6.2)     occupancy   Occupancy.
c     61 - 66  Real(6.2)     tempFactor  Temperature factor.
c     73 - 76  LString(4)    segID       Segment identifier,
c     left-justified.
c     77 - 78  LString(2)    element     Element symbol,
c     right-justified.
c     79 - 80  LString(2)    charge      Charge on the atom.
c     
      
c     include 'varcom.inc'
      integer idmy1, idmy2, i1, numatoms, ios, num_ca, num_h
      integer atom_num (1000), natoms_res
      integer i,count,j
      character (4) atom_name(1000)
      character (4) res_name(1000)
      integer (4) res_seq(1000), maxatoms
c     integer res_seq
      character (10) cdmy1, cdmy2, cdmy3,test
      character (10) cdmy4, cdmy5, cdmy6, cdmy20
      character (10) cdmy7, cdmy8, cdmy9, cdmy10
      character (4) atomname
c     parameter  numatoms = 23
c     real coordinates
      double precision X(1:1000,1:3)
      double precision L,M
c     com coordinates
      double precision X_res(1:1000,1:3), X_bc(1:1000,1:3)
      double precision X_phi(1:100,1:3)
      double precision X_psi(1:4,1:3)
      double precision B1(3),B11
      double precision B2(3),B22
      double precision B3(3),B33
      double precision B4(3)
      double precision N1(3),N2(3),phi,psi,M1(3),B2n(3),B3n(3)
      double precision B1n(3),B4n(3)
c     alpha carbon coordinates
      double precision magN1,magN2,magB2,magB3
      double precision magB1,magB4
      double precision dist(1000),dist1
      double precision crossN1(3),crossN2(3)
      character (len = 100) arg
      character ( len = 200 ) string, line
      character ( len = 200 ) cmd
      
c     Define hbond length as 2.6 Angstroms
c     hbond_length = 2.6        
      
c     convert .inpcrd file to .pdb
c     cmd = 'ambpdb -p coords.prmtop<coords.inpcrd > coords.pdb'
c     call system (cmd)
c     write (*,*)'**************************************************'
c     write (*,*)'NS, Jan, 2014. pdb_kplot converter'
c     write (*,*)'**************************************************'
      CALL getarg(1, arg)
      open (unit = 7, file = arg, status = 'old')
      open (unit = 8,file ='torsions.dat',access='append',
     &status = 'unknown')
      open (unit = 9,file ='C1_C4dist.dat',access='append',
     &status = 'unknown')                                      
      open (unit = 10,file ='conformation.dat',access='append',
     &status = 'unknown')                                        
c     write (8,*) "NDLG"
c     write (8,*) "T 'pdb_kplot...NS2014'"
c     write (8,*) "Z 100"
      
      
c     write (*,*) "NDLG"
c     write (*,*) "T 'pdb_kplot...NS2014'"
      
      
      
c     numatoms = 63
c     count number of atoms
c     numatoms = 73
      
      maxatoms = 1000
      
c     count number of atoms
      numatoms = 45
     
      count = 0
      i = 1   
      do idmy1 = 1,numatoms
c     read all lines in pdb into a string
         read ( 7, '(a)', iostat = ios ) string
c     write (*,*) trim(string)
         
c     sort all ATOMS out
         if ( string(1:4) .eq. 'ATOM' ) then
            
            i = i+1  
c     write (*,*) trim(string)
c     assign values to x,y and z coordinates here
c     read is a trick to convert string to double or other formats
            cdmy1 = (trim(string(31:38)))
            read (cdmy1, *) X(i,1)
c     write (*,*) X(i,1)
            cdmy2 = (trim(string(39:46)))
            read (cdmy2, *) X(i,2)
            cdmy3 = (trim(string(47:54)))
            read (cdmy3, *) X(i,3)
            cdmy4 = (trim(string(7:11))) 
            read (cdmy4, *) atom_num(i)
            cdmy5 = (trim(string(13:16)))
            read (cdmy5, *) atom_name(i)
c           write(*,*) atom_name(i)
            
            cdmy6 = (trim(string(23:26)))
            read (cdmy6, *) res_seq(i)
            cdmy7 = (trim(string(18:20)))
            read (cdmy7, *) res_name(i)
            
c     print only backbone for comparison with other structures.
c     turn off if statement by commenting out the if section.

             if (( atom_num(i) .eq. 39).or.
     &          (atom_num(i) .eq. 32).or.
     &          (atom_num(i) .eq. 30).or.
     &          (atom_num(i) .eq. 28).or.
     &          (atom_num(i) .eq. 26).or.
     &          (atom_num(i) .eq. 24))then
c          write(*,*) "dist",  atom_num(i),i,atom_name(i)
              X_res(i,1) = X(i,1)
              X_res(i,2) = X(i,2)
              X_res(i,3) = X(i,3) 
             endif

c   5 atoms for two torsions. 
             if ((atom_name(i).eq.'O5').and.(res_name(i).eq.'0GA').or.
     &          (atom_name(i).eq.'C1').and.(res_name(i).eq.'0GA').or.
     &          (atom_name(i).eq.'O2').and.(res_name(i).eq.'2CU').or.
     &          (atom_name(i).eq.'C2').and.(res_name(i).eq.'2CU').or.
     &           (atom_name(i).eq.'O5').and.(res_name(i).eq.'2CU')) then 
                do j = 1,3
                  X_phi(i,j)= X(i,j)
              write (*,*)"tor", X_phi(i,j), atom_num(i),i,atom_name(i),
     &   res_name(i)
                enddo
c            
             endif



             if ((atom_name(i).eq.'C1').and.(res_name(i).eq.'0GA').or.
     &          (atom_name(i).eq.'C4').and.(res_name(i).eq.'0GA')) then
                X_bc(i,1) = X(i,1)
                X_bc(i,2) = X(i,2) 
                X_bc(i,3) = X(i,3)

             endif

         endif
      enddo
   
c     finds maximum distance between atoms in rings. (chair/boat
cconformaton)
      do i=1,numatoms   
        do j = i+1,numatoms
          if ((X_res(i,1) .ne. 0).and.(X_res(j,1).ne.0)) then
          dist1 = (X(i,1)-X(j,1))**2+(X(i,2)-X(j,2))**2+
     &           (X(i,3) - X(j,3))**2
          dist(i) = sqrt(dist1)
c         write(*,*),atom_name(i),atom_name(j),dist(i),res_name(i),i
          endif
        enddo
      enddo

      do i=1,numatoms   
        do j = i+1,numatoms
          if ((X_bc(i,1) .ne. 0).and.(X_bc(j,1).ne.0)) then
          dist1 = (X(i,1)-X(j,1))**2+(X(i,2)-X(j,2))**2+
     &           (X(i,3) - X(j,3))**2
          dist(i) = sqrt(dist1)
          write(9,*)trim(arg),dist(i),res_name(i),atom_name(i)
     &,atom_name(j)
          endif
        enddo
      enddo




c     calculate vectors b1,b2,b3 for torsions from point identified
cbove. 
        
c           write (*,*) X_phi(25,1),X_phi(40,2),X_phi(3,3)

c       Phi for sucrose (atomno:39->24>1>2}
c       B1 = 39-24, B2 = 1-24, B3 = 2-1 B4 = C2->O3
c       Psi for sucrose (atomno:24->1->2->3) 


c          C1G - O5g

           B1(1) = X_phi(25,1) - X_phi(40,1) 
           B1(2) = X_phi(25,2) - X_phi(40,2) 
           B1(3) = X_phi(25,3) - X_phi(40,3) 

C          02(glycosidic)-C1G

           B2(1) = X_phi(2,1) - X_phi(25,1) 
           B2(2) = X_phi(2,2) - X_phi(25,2) 
           B2(3) = X_phi(2,3) - X_phi(25,3) 

C          C2f-O2g(glycosidic)
           B3(1) = X_phi(3,1) - X_phi(2,1) 
           B3(2) = X_phi(3,2) - X_phi(2,2) 
           B3(3) = X_phi(3,3) - X_phi(2,3) 
C          O5-C2
           B4(1) = X_phi(4,1) - X_phi(3,1) 
           B4(2) = X_phi(4,2) - X_phi(3,2) 
           B4(3) = X_phi(4,3) - X_phi(3,3) 
c        normalise all

           magB1 = sqrt(B1(1)**2 + B1(2)**2 + B1(3)**2)
           B1n(1) = B1(1)/magB1
           B1n(2) = B1(2)/magB1
           B1n(3) = B1(3)/magB1

           magB2 = sqrt(B2(1)**2 + B2(2)**2 + B2(3)**2)
           B2n(1) = B2(1)/magB2
           B2n(2) = B2(2)/magB2
           B2n(3) = B2(3)/magB2                                 

           magB3 = sqrt(B3(1)**2 + B3(2)**2 + B3(3)**2)
           B3n(1) = B3(1)/magB3
           B3n(2) = B3(2)/magB3
           B3n(3) = B3(3)/magB3 

           magB4 = sqrt(B4(1)**2 + B4(2)**2 + B4(3)**2)
           B4n(1) = B4(1)/magB4
           B4n(2) = B4(2)/magB4
           B4n(3) = B4(3)/magB4 




c      normalized cross products N1,N2 N1 = <B1 x B2> N2 = <B2 x B3>
           
          crossN1 = cross(B1n,B2n)
          crossN2 = cross(B2n,B3n)

          magN1 = sqrt(crossN1(1)**2 + crossN1(2)**2 + crossN1(3)**2)
          magN2 = sqrt(crossN2(1)**2 + crossN2(2)**2 + crossN2(3)**2)

          N1(1) = crossN1(1)
          N1(2) = crossN1(2)
          N1(3) = crossN1(3)

          N2(1) = crossN2(1)
          N2(2) = crossN2(2)
          N2(3) = crossN2(3)


c      m1 = N1 x <b2>
          M1 = cross(N1,B2n)
c      Finally L = N1.N2,  M = M1.N2 
         L = dot_product(N1,N2)
         M = dot_product(M1,N2)
c         write(*,*) "test",test
         phi = atan2(M,L)
       

c       PSI: B2 = B1, B3 = B2, B4 = B3

c      normalized cross products N1,N2 N1 = <B1 x B2> N2 = <B2 x B3>
           
          crossN1 = cross(B2,B3)
          crossN2 = cross(B3,B4)
          magN1 = sqrt(crossN1(1)**2 + crossN1(2)**2 + crossN1(3)**2)
          magN2 = sqrt(crossN2(1)**2 + crossN2(2)**2 + crossN2(3)**2)
          N1(1) = crossN1(1)
          N1(2) = crossN1(2)
          N1(3) = crossN1(3)

          N2(1) = crossN2(1)
          N2(2) = crossN2(2)
          N2(3) = crossN2(3)


c      m1 = N1 x <b2>
          M1 = cross(N1,B3n)
c      Finally L = N1.N2,  M = M1.N2 
         L = dot_product(N1,N2)
         M = dot_product(M1,N2)
          
         psi = atan2(M,L)

         
        write(*,*) "4 atoms picked in order"
        write(*,*)atom_name(40),atom_name(25),atom_name(2),atom_name(3)
         write(8,*) trim(arg),-phi*57.295, -psi*57.295
         write(*,*) "Phi=(O39-C24-O1-C2) is ",-phi*57.295
         write(*,*) "Psi=(C1-O1-C2-O5) is ",-psi*57.295
         write(*,*) "Max dist.", maxval(dist),trim(arg)

 
        
c      dist1 = (X(25,1) - X(31,1))**2 + (X(25,2) - X(31,2))**2+(X(25,3) -
c    &     X(31,3))**2
      
      
      
      
      
      end
