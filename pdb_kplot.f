        program pdb_kplot
c       This program takes a pdb file and converts to kplot
c       Modified to read only backbone for comparison. 
c       N. Sridhar -----------Nov, 2013
c       requirements: coords.pdb
c       usage: pdb_kplot
c       output: coords.kpl
        implicit none

c  PDB file structure is constant for all pdb files!
c     COLUMNS  DATA TYPE     FIELD       DEFINITION
c     --------------------------------------------------------------------------
c     1 -  6  Record name   "ATOM  "
c     7 - 11  Integer       serial      Atom serial number.
c    13 - 16  Atom          name        Atom name.
c    17       Character     altLoc      Alternate location indicator.
c    18 - 20  Residue name  resName     Residue name.
c    22       Character     chainID     Chain identifier.
c    23 - 26  Integer       resSeq      Residue sequence number.
c    27       AChar         iCode       Code for insertion of
c    residues.
c    31 - 38  Real(8.3)     x           Orthogonal coordinates for X,
c    Angstroms.
c    39 - 46  Real(8.3)     y           Orthogonal coordinates for Y,
c    Angstroms.
c    47 - 54  Real(8.3)     z           Orthogonal coordinates for Z,
c    Angstroms.
c    55 - 60  Real(6.2)     occupancy   Occupancy.
c    61 - 66  Real(6.2)     tempFactor  Temperature factor.
c    73 - 76  LString(4)    segID       Segment identifier,
c    left-justified.
c    77 - 78  LString(2)    element     Element symbol,
c    right-justified.
c    79 - 80  LString(2)    charge      Charge on the atom.
c 

c       include 'varcom.inc'
        integer idmy1, idmy2, i1, numatoms, ios, num_ca, num_h
        integer atom_num (1000)
        integer i
        character (4) atom_name(1000)
        character (4) res_name(1000)
        integer (4) res_seq(1000), maxatoms
c        integer res_seq
        character (10) cdmy1, cdmy2, cdmy3
        character (10) cdmy4, cdmy5, cdmy6, cdmy20
        character (10) cdmy7, cdmy8, cdmy9, cdmy10
        character (4) atomname
c        parameter  numatoms = 23
c        real coordinates
        double precision X(1:1000,1:3) 
c       com coordinates
        double precision X_COM(1:1000,1:3)
c      alpha carbon coordinates
        double precision X_CA(1:1000,1:3)
        double precision X2(1:1000,1:3)
        double precision X3(1:1000,1:3)
        double precision dist(10000, 10000)
        double precision xcom, ycom, zcom
        real m, c
        real theta
        real hbond_length
        character (len = 100) arg
        character ( len = 200 ) string, line
        character ( len = 200 ) cmd
        character (4) str_atom
        integer hbond_count
        real r_fit
       
c       Define hbond length as 2.6 Angstroms
        hbond_length = 2.6        
       
c       convert .inpcrd file to .pdb
c        cmd = 'ambpdb -p coords.prmtop<coords.inpcrd > coords.pdb'
c        call system (cmd)
c        write (*,*)'**************************************************'
c        write (*,*)'NS, Jan, 2014. pdb_kplot converter'
c        write (*,*)'**************************************************'
        CALL getarg(1, arg)
        open (unit = 7, file = arg, status = 'old')
        open (unit = 8, file = 'coords.kpl', status = 'unknown')

c       write (8,*) "NDLG"
c       write (8,*) "T 'pdb_kplot...NS2014'"
        write (8,*) "SE 1"
        write (8,*) "AE 2"
c       write (8,*) "Z 100"

 
c       write (*,*) "NDLG"
c        write (*,*) "T 'pdb_kplot...NS2014'"
        write (*,*) "SE 1"
        write (*,*) "AE 2"
        write (*,*) "Z 10"



c      numatoms = 63
c     count number of atoms
c       numatoms = 73
       
      maxatoms = 1000

c     count number of atoms
      numatoms = 72

      i = 1   
      do idmy1 = 1,numatoms+3
c       read all lines in pdb into a string
             read ( 7, '(a)', iostat = ios ) string
c           write (*,*) trim(string)
   
c       sort all ATOMS out
            if ( string(1:4) .eq. 'ATOM' ) then

            i = i+1  
c           write (*,*) trim(string)
c           assign values to x,y and z coordinates here
c           read is a trick to convert string to double or other formats
            cdmy1 = (trim(string(31:38)))
            read (cdmy1, *) X(i,1)
c           write (*,*) X(i,1)
            cdmy2 = (trim(string(39:46)))
            read (cdmy2, *) X(i,2)
            cdmy3 = (trim(string(47:54)))
            read (cdmy3, *) X(i,3)
            cdmy4 = (trim(string(7:11))) 
            read (cdmy4, *) atom_num(i)
            cdmy5 = (trim(string(13:16)))
            read (cdmy5, *) atom_name(i)
c            write(*,*) atom_name(i)

            cdmy6 = (trim(string(23:26)))
            read (cdmy6, *) res_seq(i)
            cdmy7 = (trim(string(18:20)))
            read (cdmy7, *) res_name(i)

c   print only backbone for comparison with other structures.
c turn off if statement by commenting out the if section.

      if ( ( atom_name(i) .eq. 'CA ') .or.
     &     ( atom_name(i) .eq. 'C ' ) .or.
     &     ( atom_name(i) .eq. 'N ')  .or.
     &     ( atom_name(i) .eq. 'O' ) .or. 
     &     ( atom_name(i) .eq. 'H' ) .or.
     &     ( atom_name(i) .eq. 'CB' ) .or.
     &     ( atom_name(i) .eq. 'HB1' ) .or.
     &     ( atom_name(i) .eq. 'HB2' ) .or.
     &     ( atom_name(i) .eq. 'HB3' ) .or.
     &     ( atom_name(i) .eq. 'HA') )  then

        
            write(8,'(a4,1x,a4,1x,a1,f8.3,1x,f8.3,1x,f8.3)')
     & 'ATOM',atom_name(i),'1',X(i,1)/100,X(i,2)/100, X(i,3)/100
            write(*,'(a4,1x,a4,1x,a1,f8.3,1x,f8.3,1x,f8.3)')
     & 'ATOM',atom_name(i),'1',X(i,1)/10,X(i,2)/10, X(i,3)/10
       endif
         endif

          enddo

                write(8,*), 'CLSE;DLG'
                write(*,*), 'CLSE;DLG'

c               write(8,*) 'rpsy,'
c               write(8,*) 'gnzl,'
c               write(8,*) 'atb,'
c               write(8,*) 'atf 3 7 5 5 1 * *'
c               write(8,*) 'atf 4 7 5 6 1 * *'
c               write(8,*) 'atf 5 7 5 2 1 * *'
c               write(8,*) 'atf 6 7 5 7 1 * *'
c               write(8,*) 'atf 7 7 5 8 1 * *'
c               write(8,*) 'atf 8 7 5 9 1 * *'
c               write(8,*) 'atf 9 7 5 1 1 * *'
c               write(8,*) 'atf 10 7 5 10 1 * *'
c               write(8,*) 'sf 1 15 3 * 0 2 1  * * * * * * *'
c               write(8,*) 'sf 2 15 3 * 0 7 1  * * * * * * *'
c               write(8,*) 'sf 3 15 3 * 0 8 1  * * * * * * *'
c               write(8,*) 'sf 4 15 3 * 0 9 1  * * * * * * *'
c               write(8,*) 'sf 5 15 3 * 0 1 1  * * * * * * *'
c               write(8,*) 'sf 6 15 3 * 0 0 1  * * * * * * *'

  
      end

 
