       module my_subs

        implicit none

       contains

       FUNCTION cross(a, b)
          DOUBLE PRECISION, DIMENSION(3) :: cross
          DOUBLE PRECISION, DIMENSION(3), INTENT(IN) :: a, b

          cross(1) = a(2) * b(3) - a(3) * b(2)
          cross(2) = a(3) * b(1) - a(1) * b(3)
          cross(3) = a(1) * b(2) - a(2) * b(1)
       END FUNCTION cross



        end module my_subs

      program pdb_kplot
c     This program takes a pdb file and converts to kplot
c     Modified to read only backbone for comparison. 
c     N. Sridhar -----------Nov, 2013
c     requirements: coords.pdb
c     usage: pdb_kplot
c     output: coords.kpl
      use my_subs
      implicit none
      
c     PDB file structure is constant for all pdb files!
c     COLUMNS  DATA TYPE     FIELD       DEFINITION
c     --------------------------------------------------------------------------
c     1 -  6  Record name   "ATOM  "
c     7 - 11  Integer       serial      Atom serial number.
c     13 - 16  Atom          name        Atom name.
c     17       Character     altLoc      Alternate location indicator.
c     18 - 20  Residue name  resName     Residue name.
c     22       Character     chainID     Chain identifier.
c     23 - 26  Integer       resSeq      Residue sequence number.
c     27       AChar         iCode       Code for insertion of
c     residues.
c     31 - 38  Real(8.3)     x           Orthogonal coordinates for X,
c     Angstroms.
c     39 - 46  Real(8.3)     y           Orthogonal coordinates for Y,
c     Angstroms.
c     47 - 54  Real(8.3)     z           Orthogonal coordinates for Z,
c     Angstroms.
c     55 - 60  Real(6.2)     occupancy   Occupancy.
c     61 - 66  Real(6.2)     tempFactor  Temperature factor.
c     73 - 76  LString(4)    segID       Segment identifier,
c     left-justified.
c     77 - 78  LString(2)    element     Element symbol,
c     right-justified.
c     79 - 80  LString(2)    charge      Charge on the atom.
c     
      
c     include 'varcom.inc'
      integer idmy1, idmy2, i1, numatoms, ios, num_ca, num_h
      integer atom_num (1000), natoms_res
      integer i,count,j
      character (4) atom_name(1000)
      character (4) res_name(1000)
      integer (4) res_seq(1000), maxatoms
c     integer res_seq
      character (10) cdmy1, cdmy2, cdmy3,test
      character (10) cdmy4, cdmy5, cdmy6, cdmy20
      character (10) cdmy7, cdmy8, cdmy9, cdmy10
      character (4) atomname
      character (5)  cistrans
c     parameter  numatoms = 23
c     real coordinates
      double precision X(1:1000,1:3)
      double precision L,M
c     com coordinates
      double precision X_res(1:1000,1:3)
      double precision X_phi(1:100,1:3)
      double precision X_psi(1:4,1:3)
      double precision B1(3),B11
      double precision B2(3),B22
      double precision B3(3),B33
      double precision B4(3)
      double precision N1(3),N2(3),phi,psi,M1(3),B2n(3),B3n(3)
      double precision B1n(3),B4n(3)
c     alpha carbon coordinates
      double precision magN1,magN2,magB2,magB3
      double precision magB1,magB4
      double precision dist1,dist2,dist3,dist4
      integer count_c, count_t
      double precision crossN1(3),crossN2(3)
      character (len = 100) arg
      character ( len = 200 ) string, line
      character ( len = 200 ) cmd
      
c     Define hbond length as 2.6 Angstroms
c     hbond_length = 2.6        
      
c     convert .inpcrd file to .pdb
c     cmd = 'ambpdb -p coords.prmtop<coords.inpcrd > coords.pdb'
c     call system (cmd)
c     write (*,*)'**************************************************'
c     write (*,*)'NS, Jan, 2014. pdb_kplot converter'
c     write (*,*)'**************************************************'
      CALL getarg(1, arg)
      open (unit = 7, file = arg, status = 'old')
      
      maxatoms = 1000
      
c     count number of atoms
      numatoms = 45
     
      count = 0
      i = 1   
      do idmy1 = 1,numatoms
c     read all lines in pdb into a string
         read ( 7, '(a)', iostat = ios ) string
c     write (*,*) trim(string)
         
c     sort all ATOMS out
         if ( string(1:4) .eq. 'ATOM' ) then
            
            i = i+1  
c     write (*,*) trim(string)
c     assign values to x,y and z coordinates here
c     read is a trick to convert string to double or other formats
            cdmy1 = (trim(string(31:38)))
            read (cdmy1, *) X(i,1)
c     write (*,*) X(i,1)
            cdmy2 = (trim(string(39:46)))
            read (cdmy2, *) X(i,2)
            cdmy3 = (trim(string(47:54)))
            read (cdmy3, *) X(i,3)
            cdmy4 = (trim(string(7:11))) 
            read (cdmy4, *) atom_num(i)
            cdmy5 = (trim(string(13:16)))
            read (cdmy5, *) atom_name(i)
c           write(*,*) atom_name(i)
            
            cdmy6 = (trim(string(23:26)))
            read (cdmy6, *) res_seq(i)
            cdmy7 = (trim(string(18:20)))
            read (cdmy7, *) res_name(i)
            
c     pick O and H atoms of peptide bonds ith other structures.
c     turn off if statement by commenting out the if section.
c        write(*,*) X(i,1),i,atom_name(i)

        endif
      enddo

c     find distance between H-O peptides. atoms . (chair/boat
cconformaton)
      
      dist2 = sqrt( (X(8,1) - X(10,1))**2 + (X(8,2) - X(10,2))**2+
     & (X(8,3) - X(10,3))**2  )
c     write(*,*) dist1
      dist3 = sqrt( (X(15,1) - X(17,1))**2 + (X(15,2) - X(17,2))**2+    
     & (X(15,3) - X(17,3))**2  )                                        
c     write(*,*) dist2   

      dist4 = sqrt( (X(22,1) - X(24,1))**2 + (X(22,2) - X(24,2))**2+    
     & (X(22,3) - X(24,3))**2  )                                        
c     write(*,*) dist3

      dist1 = sqrt( (X(29,1) - X(3,1))**2 + (X(29,2) - X(3,2))**2+    
     & (X(29,3) - X(3,3))**2  )                                        
c      write(*,*) dist4

      count_c = 0
      count_t = 0

c     ASSIGN CIS OR TRANS
      if (dist1.lt.2.5) then
      cistrans(1:2) = 'C'  
      count_c = count_c + 1
      else 
      cistrans(1:2) = 'T'  
      count_t = count_t + 1
      endif

      if (dist2.lt.2.5) then
      cistrans(2:3) = 'C'
      count_c = count_c + 1
      else 
      cistrans(2:3) = 'T'
      count_t = count_t + 1
      endif

      if (dist3.lt.2.5) then
      cistrans(3:4) = 'C' 
      count_c = count_c + 1
      else 
      cistrans(3:4) = 'T' 
      count_t = count_t + 1
      endif

      if (dist4.lt.2.5) then
      cistrans(4:5) = 'C'
      count_c = count_c + 1
      else 
      cistrans(4:5) = 'T' 
      count_t = count_t + 1
      endif
      
      write(*,*) trim(arg),'  ',  cistrans(1:5),count_c,count_t

c      dist1 = (X(25,1) - X(31,1))**2 + (X(25,2) - X(31,2))**2+(X(25,3) -
c    &     X(31,3))**2
      
      
      
      
      
      end
